# Scope

Rechnungslegung und Anbindung an Shopsysteme

# Documentation / Contribute
https://fakturama.atlassian.net/wiki/display/FAK/Developer%27s+Corner

# Issue Tracker
https://fakturama.atlassian.net/projects/FAK/issues

# Authors
* Ralf Heydenreich <ralf.heydenreich@fakturama.net>
* Christian Fischer <fakturama@computerlyrik.de>
